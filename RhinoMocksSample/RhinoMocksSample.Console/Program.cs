﻿using Microsoft.Practices.ServiceLocation;
using RhinoMocksSample.ConsoleApp.Services;
using System;

namespace RhinoMocksSample.ConsoleApp
{
    class Program
    {
        /// <summary>
        /// Tu poleci wyjątek - metoda w PersonQuery.GetById nie jest jeszcze zaimplementowana 
        /// i rzuca wyjątek.
        /// Ale potwierdza to że nasz service locator zadziałał poprawnie
        /// </summary>
        static void Main(string[] args)
        {
            ContainerBootstrapper.ConfigureContainer();

            var fullNameService = ServiceLocator.Current.GetInstance<FullNameService>();
            var personFullName = fullNameService.FormatFullName(5);

            Console.WriteLine(personFullName);

            Console.ReadKey();
        }
    }
}
