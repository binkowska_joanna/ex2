﻿using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using RhinoMocksSample.ConsoleApp.Services;
using RhinoMocksSample.Core.Queries;
using RhinoMocksSample.Core.Services;
using RhinoMocksSampleConsoleApp.Queries;

namespace RhinoMocksSample.ConsoleApp
{
    /// <summary>
    /// Inicjalizacja dependency injection
    /// </summary>
    public static class ContainerBootstrapper
    {
        internal static readonly IUnityContainer _container = new UnityContainer();

        public static void ConfigureContainer()
        {
            ServiceLocator.SetLocatorProvider(() => new UnityServiceLocator(_container));
            
            _container.RegisterType<IFullNameService, FullNameService>();
            _container.RegisterType<IPersonQuery, PersonQuery>();
        }
    }
}
