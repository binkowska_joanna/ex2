﻿using RhinoMocksSample.Core.Queries;
using RhinoMocksSample.Core.Services;

namespace RhinoMocksSample.ConsoleApp.Services
{
    public class FullNameService : IFullNameService
    {
        private IPersonQuery _personQuery;
        
        /// <summary>
        /// FullNameService w naszym "nieidealnym" świecie potrzebuje pobrać Person z bazy
        /// W konstruktor wstrzykniemy IPersonQuery, które reprezentuje interfejs pobierania danych z bazy
        /// </summary>
        /// <param name="personQuery"></param>
        public FullNameService(IPersonQuery personQuery)
        {
            _personQuery = personQuery;
        }

        public string FormatFullName(int id)
        {
            var person = _personQuery.GetById(id);

            if (string.IsNullOrEmpty(person.FirstName)
                || string.IsNullOrEmpty(person.LastName))
                return null;

            if (string.IsNullOrEmpty(person.MiddleName))
                return string.Format("{0} {1}", person.FirstName, person.LastName);

            return string.Format("{0} {1} {2}",
                person.FirstName,
                person.MiddleName,
                person.LastName);
        }
    }
}
