﻿using RhinoMocksSample.Core.Entities;
using RhinoMocksSample.Core.Queries;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RhinoMocksSampleConsoleApp.Queries
{
    public class PersonQuery : IPersonQuery
    {
        /// <summary>
        /// W prawdziwej aplikacji PersonQuery pobiera obiekt Persona z bazy danych po Id
        /// Póki co rzucamy wyjątek - metoda nie jest jeszcze zaimplementowana
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Person GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
