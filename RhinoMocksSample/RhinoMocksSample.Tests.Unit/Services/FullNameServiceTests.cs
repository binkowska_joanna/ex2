﻿using Microsoft.Practices.ServiceLocation;
using NUnit.Framework;
using Rhino.Mocks;
using RhinoMocksSample.ConsoleApp;
using RhinoMocksSample.ConsoleApp.Services;
using RhinoMocksSample.Core.Entities;
using RhinoMocksSample.Core.Queries;
using System.Collections.Generic;

namespace RhinoMocksSample.Tests.Unit
{
    [TestFixture]
    public class FullNameServiceTests
    {
        private FullNameService _fullNameService;

        [OneTimeSetUp] // Wywołanie raz, na początku przed wszystkimi testami
        public void TestFixtureSetUp()
        {
            ContainerBootstrapper.ConfigureContainer();
        }

        /// <summary>
        /// Testujemy realną klasę FullNameService, ale chcemy zamokować IPersonQuery w tej klasie
        /// Czyli IPersonQuery utworzymy przez RhinoMock i przekażemy je w konstruktorze do FullNameService
        /// Metodę GetById imitujemy poprzez Stub żeby zwracała oczekiwanego przez nas Persona
        /// </summary>
        [Test]
        public void FormatFullName_ArgumentsProvided_ReturnExpectedResult()
        {
            var personQueryMock = MockRepository.GenerateStrictMock<IPersonQuery>();
            personQueryMock.Stub(pqm => pqm.GetById(Arg<int>.Is.Anything))
                .Return(new Person
                {
                    FirstName = "Joanna",
                    MiddleName = null, 
                    LastName = "Kowalska"
                });

            _fullNameService = new FullNameService(personQueryMock);

            var actualResult = _fullNameService.FormatFullName(Arg<int>.Is.Anything);

            Assert.AreEqual(actualResult, "Joanna Kowalska");
        }
    }
}
