﻿namespace RhinoMocksSample.Core.Services
{
    public interface IFullNameService
    {
        string FormatFullName(int id);
    }
}
